<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wdb' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'whost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'VhsdI1!%!,v|E)6Izz+PA8xPu60B%OdH! R-}c5Ou~m5sa&CDQS;Oaj$z,nyR|=&' );
define( 'SECURE_AUTH_KEY',  'vxM9{Z&JZ(y^C9Vh~-2XT)nS`.gKwC7N>}vSZn,HLB-fOcc$,#9>z?q;?GHPR!M=' );
define( 'LOGGED_IN_KEY',    '$;koe?w1h1f0iSRbmK5_D7F<R{f%m7]&E,8r[gCb(*5 A|gRJt`k.xnx8y[oP-@c' );
define( 'NONCE_KEY',        'kM9,.3G]~k/esag_DEjDrXb#B))qa)h0so$B8]^T9zMz8fRwRx6N;e!DW6&w9<dL' );
define( 'AUTH_SALT',        '/b7%vjiTDBD-xZlZOCynGq%QC)#Nmfsh*w<D%+1DzI<B< xwJ<M>1dx_hid gK7K' );
define( 'SECURE_AUTH_SALT', 'qk)^1s|=h0YdQ6Su8R@BOgm2LF%zhH`86!Oy0#023`v{5l#QQr*@uh$R>)&=={P]' );
define( 'LOGGED_IN_SALT',   ']*5(#S+@v%;Ts(9GWPKucF@8,KHAZa}ZS4|#K@G=P$6OGaCIyt~|wJ^k)pvf=xjx' );
define( 'NONCE_SALT',       'Qk^x?`(kAOOSZ>i2Rr.tfeZr}}DTxz372g_?+)4,2k1v~Cdch~(>33wQ:m@de|kM' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

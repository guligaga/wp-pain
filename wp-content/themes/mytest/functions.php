<?php
// wp_register_style('my_style', get_template_directory_uri() . 'style.css', array('my_reset'), '1.47', 'screen');

// wp_enqueue_style('my_style');
add_theme_support( 'post-thumbnails' );

add_action( 'wp_enqueue_scripts', 'my_enqueue_style' );

function my_enqueue_style() {
    wp_enqueue_style( 'my_style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'font-style', 'https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600|Nunito+Sans:300,400,600,700,900&display=swap' );
    }
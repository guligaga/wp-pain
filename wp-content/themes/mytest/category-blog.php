<?php get_header(); ?>
    <section class="blog-wrapper">
        <h1 class="blog-title">All my thoughts and experience</h1>
        <div class="blog-posts">
        <?php if( have_posts() ){ while( have_posts() ){ the_post(); ?>

            <div class="post-container">
            <div class="post-picture"><?php the_post_thumbnail() ?></div>
            <div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
                <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                <?php the_content(); ?>
                <?php the_author();?>
                <?php the_date();?>
            </div>
            </div>

            <?php } 
        } 
        ?>
    </section>
<?php get_footer(); ?>

